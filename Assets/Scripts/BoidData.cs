﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public struct BoidData 
{
    public Vector3 velocity;
    public Vector3 acceleration;
    public float perceptionRadius;

}
