﻿using UnityEngine;


public class Boid : MonoBehaviour
{
    [SerializeField]
    private BoidData boidData;
    private void Awake()
    {
        boidData.velocity = new Vector3(Random.Range(-2, 2) + 0.01f, Random.Range(-2, 2) + 0.01f, Random.Range(-2, 2) + 0.01f);
        boidData.acceleration = Vector3.zero;
    }
    private void Update()
    {
        boidData.acceleration = Vector3.zero;
        Vector3 alignment = align();
        Vector3 cohesion = this.cohesion();
        Vector3 seperation = this.seperation();
        if(IsCollision())
        {
            Debug.Log("Detect");
            Vector3 collisionAvoidDir = ObstacleRays();
            Vector3 collisionAvoidForce =(collisionAvoidDir.normalized*BoidSpawner.boidSpawner.maxSpeed) * BoidSpawner.boidSpawner.avoidweight;
            boidData.velocity = collisionAvoidForce - boidData.velocity;
            boidData.acceleration += collisionAvoidForce;
        }
        boidData.acceleration += alignment * BoidSpawner.boidSpawner.alignweight;
        boidData.acceleration += cohesion * BoidSpawner.boidSpawner.cohesionweight;
        boidData.acceleration += seperation * BoidSpawner.boidSpawner.sperateweight;
        UpdateBoid();


    }

    private bool IsCollision()
    {
        RaycastHit hit;
        if (Physics.SphereCast(transform.position, boidData.perceptionRadius, transform.forward, out hit, 5.0f, BoidSpawner.boidSpawner.obstacle))
        {
            return true;
        }
        return false;
    }
    private Vector3 SteerToward(Vector3 v)
    {
        Vector3 velocity = v.normalized * BoidSpawner.boidSpawner.maxSpeed;
        return Vector3.ClampMagnitude(velocity, BoidSpawner.boidSpawner.maxForce);
    }
    Vector3 ObstacleRays()
    {
        Vector3[] rayDirections = BoidRayCast.directions;

        for (int i = 0; i < rayDirections.Length; i++)
        {
            Vector3 dir = transform.TransformDirection(rayDirections[i]);
            Ray ray = new Ray(transform.position, dir);
            if (!Physics.SphereCast(ray,boidData.perceptionRadius,5.0f, BoidSpawner.boidSpawner.obstacle))
            {
                return dir;
            }
        }

        return transform.forward;
    }
    private Vector3 align()
    {
        float dist = 0.0f;
        Vector3 steering = Vector3.zero;
        int count = 0;
        foreach (Boid boid in BoidSpawner.boidSpawner.boids)
        {
            if (boid != this)
            {
                dist = Vector3.Distance(transform.position, boid.transform.position);
                if (dist < boidData.perceptionRadius)
                {
                    steering += boid.boidData.velocity;
                    count++;
                }
            }
        }
        if (count > 0)
        {
            steering /= (float)count;
            steering = steering.normalized * BoidSpawner.boidSpawner.maxSpeed;
            steering -= boidData.velocity;
            Vector3.ClampMagnitude(steering, BoidSpawner.boidSpawner.maxForce);


        }
        return steering;

    }

    private Vector3 cohesion()
    {
        float dist = 0.0f;
        Vector3 steering = Vector3.zero;
        int count = 0;
        foreach (Boid boid in BoidSpawner.boidSpawner.boids)
        {
            if (boid != this)
            {
                dist = Vector3.Distance(transform.position, boid.transform.position);
                if (dist < boidData.perceptionRadius)
                {
                    steering += boid.transform.position;
                    count++;
                }
            }
        }
        if (count > 0)
        {
            steering /= (float)count;
            steering -= transform.position;
            steering = steering.normalized * BoidSpawner.boidSpawner.maxSpeed;
            steering -= boidData.velocity;
            Vector3.ClampMagnitude(steering, BoidSpawner.boidSpawner.maxForce);


        }
        return (steering);

    }

    private Vector3 seperation()
    {
        float dist = 0.0f;
        Vector3 steering = Vector3.zero;
        int count = 0;
        foreach (Boid boid in BoidSpawner.boidSpawner.boids)
        {
            if (boid != this)
            {
                dist = Vector3.Distance(transform.position, boid.transform.position);
                if (dist < 5)
                {
                    Vector3 diff = (transform.position - boid.transform.position).normalized;
                    steering += diff;
                    count++;
                }
            }
        }
        if (count > 0)
        {
            steering /= (float)count;
            steering = steering.normalized * BoidSpawner.boidSpawner.maxSpeed;
            steering -= boidData.velocity;
            Vector3.ClampMagnitude(steering, BoidSpawner.boidSpawner.maxForce);

        }
        return steering;

    }

    private void UpdateBoid()
    {
        transform.position += boidData.velocity * Time.deltaTime;

        boidData.velocity += boidData.acceleration * Time.deltaTime;
        Vector3 dir = boidData.velocity.normalized;
        transform.forward = dir;
        SteerToward(boidData.velocity);


    }

 
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, boidData.perceptionRadius);
        for (int i = 0; i < BoidRayCast.directions.Length; i++)
            Gizmos.DrawRay(transform.position, BoidRayCast.directions[i]);
    }

}
