﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoidSpawner : MonoBehaviour
{
    public LayerMask obstacle;
    public float cohesionweight;
    public float sperateweight;
    public float alignweight;
    public float avoidweight;
    public float maxSpeed;
    public float maxForce;


    public int count;
    public Boid boid;
    public int boundSpawner;
    public static BoidSpawner boidSpawner;
    public  List<Boid> boids;
    public void Awake()
    {
        boidSpawner = this;
    }


    public void Start()
    {
        for(int i =0;i<count;i++)
        {
            Vector3 posRandom = Random.insideUnitSphere * boundSpawner;
            Boid obj = Instantiate(boid, transform.position + new Vector3(posRandom.x, posRandom.y, posRandom.z), Quaternion.identity);
            obj.transform.SetParent(this.transform);
            boids.Add(obj);
        }

    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.gray;
        Gizmos.DrawWireSphere(transform.position, boundSpawner);
    }

}
